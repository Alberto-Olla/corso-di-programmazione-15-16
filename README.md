# CORSO DI PROGRAMMAZIONE 2015/2016 - TUTORATO #
# FACOLTA' DI INFORMATICA - UNIVERSITA' DEGLI STUDI DI ROMA TOR VERGATA #

# Lezioni #
**20/01/16** Ordinamento di un array di stringhe con Bubble-Sort (13-bubble_sort_string.c). Differenze tra matrici e array di puntatori. Ripasso e ssercitazione sui puntatori e gli operatori * e &. Introduzione alle funzioni malloc(), free() e sizeof().

**11/01/16** Soluzione della Simulazione esame 2 (soluzione con main). La funzione di libreria fgets() per l’inserimento di stringhe da tastiera. Le matrici bidimensionali come array di array: come si dichiarano e come si accede ai suoi elementi. Esercizio: data una matrice quadrata binaria trovare la più grande sotto matrice quadrata costituita di soli 1.

# Teoria #
## Matrici - Array Bidimensionali ##
Una matrice a due dimensioni è un array bidimensionale di dimensioni fissate a priori, identificato da due indici, i cui elementi sono tutti dello stesso tipo (base).

![Screen Shot 2016-01-11 at 18.57.45.png](https://bitbucket.org/repo/zdXEBk/images/731478451-Screen%20Shot%202016-01-11%20at%2018.57.45.png)

Similmente all'array è identificata dal tipo degli elementi, da un nome, dal numero di righe (N) e numero di colonne (M). In generale, una matrice bidimensionale
si dice NxM se costituita da N righe e M colonne con N!=M, quadrata se N = M e quindi N righe e N colonne. Di fatto le matrici possono essere viste come 
vettore di vettori.

    tipo_base nome_matrice[#righe][#colonne];
    int matrice[N][M];

dove N e M sono state definite come costanti tramite #define, con int indichiamo che gli elementi sono di tipo intero, con "matrice" identifichiamo il nome della stessa.

Per inserire elementi all'interno della matrice:

    int matrice[N][M] = {{1, 1, 0},
                        {0, 1, 1},
                        {0, 1, 1}};

Per accedere agli elementi della matrice si usa l'operatore di indicizzazione:
    
    int a = matrice[3][4];
    
In questo caso si memorizza in "a" l'elemento di riga 3 e colonna 4. In generale:

    int a = matrice[indice_di_riga][indica_di_colonna];

Dove:

* 0 <= indice_di_riga < #righe 
* 0 <= indica_di_colonna < #colonne.

Chiaramente si può utilizzare un doppio ciclo for per accedere a tutti gli elementi della matrice. Con il primo ciclo si cicla sulle righe, con il secondo  si cicla sulle colonne:

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; j++) {
            printf(" %d ", matrice[i][j]);
        }
        printf("\n");
    }

In questo semplice esempio si stampano tutti i valori della matrice a partire dagli indici i = 0 (di riga), j = 0 (di colonna). Ovviamente il doppio ciclo for
si può utilizzare anche per inserire valori all'interno della matrice:

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; j++) {
            matrice[i][j] = i+j;
        }
    }

Ovvero si memorizza nella cella identificata da riga i e colonna j la somma di i+j. Passando la matrice 2d come parametro di funzione, bisogna ricordarsi di specificare almeno il numero di colonne nel prototipo. Solo il primo indice (di riga), infatti, può rimanere vuoto.

    void funzione(int matrice[][M]) {
        // codice
    }

Il motivo risulta anche banale. Quando si passa un array ad una funzione viene passato in realtà l'indirizzo del primo elemento. E poiché gli array contengono elementi dello stesso tipo, scorrere gli stessi al suo interno risulta sufficientemente semplice, avanzando di blocco in blocco di xbyte (es. gli int occupano 4byte, i char 1byte). Per le matrici il discorso è differente: infatti essendo vettori di vettori il compilatore necessita di conoscere la seconda dimensione per muoversi nell'array da un puntatore al successivo (da una "riga" alla successiva).
 
### Esercizio ###
Data in input una matrice quadrata binaria di dimensioni fissate, trovare la più grande sottomatrice quadrata costituta da soli 1.
(riferimenti teorici - Programmazione dinamica: [https://it.wikipedia.org/wiki/Programmazione_dinamica](Link URL))
**Per ulteriori spiegazioni si rimanda al sito del docente.**

# Code Style #
## Commenti ##

Molti sviluppatori ritengono che descrivere un componente del codice in modo esteso sia inutile o semplicemente una perdita di tempo. Niente di più sbagliato: infatti da un punto di vista della riusabilità  del codice è certamente molto più pratico e veloce che trascorrere ore su Skype cercando di aiutare uno sviluppatore che sta leggendo per la prima volta il nostro codice. I commenti descrittivi sono in assoluto i più utili perché aiutano l'usabilità  e ci risparmiano una notevole fatica (estratto da Commentare il codice, Gabriele Romanato, Edit). Vi invito a commentare non solo le operazioni più importanti ma anche le funzioni. Di seguito un esempio (dove il doppio slash viene utilizzato per inserire commenti):


    /*
    * Calcola … e poi divide … per restituire la derivata seconda di x
    */
    float NomeFunzione(parametri…) {
	    ... codice ...
	    ... codice ...
	    // eventuale commento sull’operazione a seguire
	    ... codice ...
	    // eventuale commento sull’operazione a seguire
	    ... codice ...
    }
## Indentazione ##

E' cosa buona e giusta che si inizi ad indentare correttamente il codice. Due sono le best practice consigliate: la tabulazione o i quattro spazi. L’indentazione, infatti, consiste nel precedere le righe di codice con un certo numero di spazi con lo scopo di evidenziare i blocchi di codice. Ignorata dal compilatore, serve al programmatore per cogliere visivamente la struttura del programma e si effettua mentre lo si sviluppa, non successivamente per renderlo “bello". (estratto da Complementi 1, Claudio Fornaro, Politecnico di Torino).
Esempio indentazione sbagliata: 

    /*
    * Codice di esempio 
    */
    struct triangolo{
    punto p0, p1, p2;
    };
    typedef struct triangolo triangolo;

Esempio indentazione corretta: 
    
    /*
    * Codice di esempio 
    */
    struct triangolo{
         punto p0, p1, p2;
    };
    typedef struct triangolo triangolo;

Questo migliora enormemente la leggibilità e consente di capire immediatamente quando inizia e finisce un blocco di codice. Python peraltro non fa uso di parentesi graffe ma esclusivamente dell’indentazione. 

Un esempio in Python:
    
    ## Codice di esempio
    def plot_target_frequency(t):
        target_frequency = scipy.stats.itemfreq(t)
        print target_frequency
    
        plt.figure(facecolor='w')

        for n, row in enumerate(target_frequency):
            if row[0] < 0:
                label = 'Benigno'
        plt.gca().axes.xaxis.set_ticklabels([])

In questo caso tutto il codice fa parte della funzione plot_target_frequency, la condizione if è all’interno del ciclo for mentre la funzione plt è al suo esterno.


## Funzioni ##
Quando si affronta un problema più o meno "complesso" il migliore approccio da utilizzare è di dividere il problema in tanti sottoproblemi più semplici di cui siamo in grado di trovare una soluzione “semplice". Nel nostro caso, non solo aiuta a risolvere il problema, ma anche in un’ottica di riuso del codice risulta essere particolarmente utile. Pertanto siete invitati a sviluppare funzioni secondarie a parte.
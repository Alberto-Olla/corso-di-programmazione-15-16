//
// Corso di Programmazione - Tutorato
// AA 2015/2016
// Facoltà di Informatica
// Università degli Studi di Roma Tor Vergata
//
// Simulazione d'esame 1
// @version 0.1
// @link http://www.gianlucarossi.net/programmazione-dei-calcolatori/
//
// @author Daniele Pasquini
// @email daniele.pasquini@students.uniroma2.eu, psqdnl@hotmail.it, psqdnl@gmail.com
//

#include <math.h>

/**
 * Costanti
 */
static int const MAX_ITERAZIONI = 100;

struct punto {
    float x, y;
};
typedef struct punto punto;

struct triangolo {
    punto p0, p1, p2;
};
typedef struct triangolo triangolo;

/*
 * Prototipi
 */
float AreaTriangoloIsoscele(triangolo);
float AreaTriangoloIsoscele_Erone(triangolo); // calcolo dell'area alternativo con Erone
float AltezzaTriangoloIsoscele(float, float);
float DistanzaEuclidea2D(punto, punto);
int VerificaTriangolo(float, float, float);

/* Prototipi delle funzioni matematiche alternative, sostituiscono le funzioni presenti nella liberia math.
 * Sono riportate a titolo informativo
 */
float RadiceQuadrata_Parametrica(float, int);
float RadiceQuadrata(float);
float Potenza(float, int);


/*
 * Se il triangolo in input è isoscele ne calcola l'area, altrimenti torna -1
 *
 * @param triangolo definito da tre punti euclidei
 * @return l'area del triangolo se isoscele (il triangolo equilatero è un triangolo isoscele),
 *          altrimenti -1 per indicare un triangolo scaleno o nel caso in cui non sia un triangolo
 */
float AreaTriangoloIsoscele(triangolo t) {

    float h; // altezza
    float l; // lato
    float b; // base
    float A; // area

    float lato_ab = DistanzaEuclidea2D(t.p0, t.p1);
    float lato_ac = DistanzaEuclidea2D(t.p0, t.p2);
    float lato_bc = DistanzaEuclidea2D(t.p1, t.p2);

    if(VerificaTriangolo(lato_ab, lato_bc, lato_ac)) { // verifica se è un triangolo
        if(lato_ab != lato_ac && lato_ab != lato_bc && lato_ac != lato_bc) { // se triangolo scaleno torna -1 e non calcola l'area
            return -1;
        } else if(lato_ab == lato_ac) { // è un triangolo isoscele
            l = lato_ab;
            b = lato_bc;
        } else if(lato_ab == lato_bc) { // è un triangolo isoscele
            l = lato_ab;
            b = lato_ac;
        } else { // è un triangolo isoscele
            l = lato_bc;
            b = lato_ab;
        }
    } else
        return -1; // non è un triangolo, non calcola l'area

    h = AltezzaTriangoloIsoscele(l, b);
    A = b * (h/2);
    return A;

}

/*
 * Calcola la distanza euclidea utilizzando la libreria math
 * per le funzioni radice quadrata e potenza
 *
 * @param m, n punti euclidei
 * @return la distanza euclidea bidimensionale
 */
float DistanzaEuclidea2D(punto m, punto n) {
    float diff_x = m.x - n.x;
    float diff_y = m.y - n.y;
    float diff_sq_x = pow(diff_x, 2);
    float diff_sq_y = pow(diff_y, 2);
    float eu_2d = sqrt(diff_sq_x + diff_sq_y);

    return eu_2d;
}

/*
 * Calcola l'altezza di un triangolo isoscele relativa alla base
 * utilizzando la libreria math per le funzioni radice quadrata e potenza
 *
 * @param l lato obliquo
 * @param b base del triangolo
 * @return altezza del triangolo relativa alla base
 */
float AltezzaTriangoloIsoscele(float l, float b) {
    return sqrt(pow(l, 2) - (pow(b, 2)/4));
}

/*
 * Condizione di esistenza del triangolo:
 * ogni lato deve essere maggiore della differenza degli altri due e minore della loro somma
 *
 * @author Fabiano Golluscio
 * @author Luigi Perfetto
 * @contributor Faliero Rogo
 * @contributor Andrea D'Ubaldo
 *
 */
int VerificaTriangolo(float lato_ab, float lato_bc, float lato_ac) {
    if ( lato_ab > lato_bc-lato_ac && lato_ab < lato_bc+lato_ac &&
            lato_bc < lato_ab+lato_ac && lato_bc > lato_ab-lato_ac &&
            lato_ac < lato_bc+lato_ab && lato_ac>lato_bc-lato_ab) {
        return 1;
    } else {
        return -1;
    }
}

/************************************************************\
                CALCOLO AREA ALTERNATIVO
\************************************************************/

/*
 * Se il triangolo in input è isoscele ne calcola l'area con la formula di Erone, altrimenti torna -1
 *
 * @param triangolo definito da tre punti euclidei
 * @return l'area del triangolo se isoscele (il triangolo equilatero è un triangolo isoscele),
 *          altrimenti -1 per indicare un triangolo scaleno
 *
 * @author Leonardo Murru
 * @author Orlando Parente
 * @author Manuel Gallucci
 *
 */
float AreaTriangoloIsoscele_Erone(triangolo t) {
    float lato_ab = DistanzaEuclidea2D(t.p0, t.p1);
    float lato_ac = DistanzaEuclidea2D(t.p0, t.p2);
    float lato_bc = DistanzaEuclidea2D(t.p1, t.p2);

    if(VerificaTriangolo(lato_ab, lato_bc, lato_ac)) { // verifica se è un triangolo
        if (lato_ab != lato_ac && lato_ab != lato_bc && lato_ac != lato_bc) { // se è un triangolo scaleno
            return -1;
        } else {
            float semi_perimetro = (lato_ab + lato_ac + lato_bc) / 2; // semiperimetro
            // calcola l'area con la formula di Erone
            return sqrt(semi_perimetro * (semi_perimetro - lato_ab) * (semi_perimetro - lato_bc) *
                        (semi_perimetro - lato_ac));
        }
    } else {
        return -1; // non è un triangolo, non calcola l'area
    }
}

/************************************************************\
              FUNZIONI MATEMATICHE ALTERNATIVE
\************************************************************/

/**
 * Calcola la potenza.
 * Accetta solo decimali come base
 *
 * @param base
 * @param esponente
 * @return risultato operazione potenza
 *
 * @contributor Lorenzo Monesi
 *
 */
float Potenza(float base, int esponente) {
    float risultato;
    if(base == 0) return 0;

    // Verifica se l'esponente è negativo
    if(esponente < 0) {
        risultato = 1/base;
        base = 1/base;
        esponente = -esponente;
    } else if(esponente > 0){
        risultato = base;
    } else {
        return 1; // esponente = 0;
    }

    for(int i = 0; i < esponente - 1; i++) {
        risultato *= base;
    }

    return risultato;
}

/*
 * Calcola la radice quadrata di un numero con il metodo babilonese
 * ed un numero di iterazioni fissato dal chiamante.
 *
 * @param x valore di cui calcolare la radice quadrata
 * @param max_iterazioni numero di iterazioni dell'algoritmo
 * @return radice quadrata del valore in input
 *
 * @author Andrea D'Ubaldo
 * @author Fabiano Golluscio
 * @author Luigi Perfetto
 * @author Beatrice Bottiglieri
 * @author Lorenzo Monesi
 * @author Adriano D'Aguanno
 * @author Daniele D'Arienzo
 * @author Davide Pataracchia
 * @author Leonardo Tamiano
 *
 */
float RadiceQuadrata_Parametrica(float x, int max_iterazioni) {
    float radice = x/2;
    int i = 0;

    while(i < max_iterazioni) {
        radice=(radice + (x/radice))/2;
        i++;
    }

    return radice;
}

/*
 * Calcola la radice quadrata di un numero con il metodo babilonese
 * ed un numero di iterazioni MAX_ITERAZIONI
 *
 * @param x valore di cui calcolare la radice quadrata
 * @return radice quadrata del valore in input
 *
 * @author Andrea D'Ubaldo
 * @author Fabiano Golluscio
 * @author Luigi Perfetto
 * @author Beatrice Bottiglieri
 * @author Lorenzo Monesi
 * @author Adriano D'Aguanno
 * @author Daniele D'Arienzo
 * @author Davide Pataracchia
 * @author Leonardo Tamiano
 *
 */
float RadiceQuadrata(float x) {
    float radice = x/2;
    int i = 0;

    while(i < MAX_ITERAZIONI) {
        radice=(radice + (x/radice))/2;
        i++;
    }

    return radice;
}
//
// Corso di Programmazione - Tutorato
// AA 2015/2016
// Facoltà di Informatica
// Università degli Studi di Roma Tor Vergata
//
// Simulazione d'esame 2
// @version 0.1
// @link http://www.gianlucarossi.net/programmazione-dei-calcolatori/
//
// @author Daniele Pasquini
// @email daniele.pasquini@students.uniroma2.eu, psqdnl@hotmail.it, psqdnl@gmail.com
//

/**
 * Header delle librerie utilizzate
 */
#include <string.h>
#include <stdio.h>
#include <assert.h>

/**
 * Costanti
 */
static int const PRIMO_CARATTERE_MAIUSCOLO = 65;
static int const ULTIMO_CARATTERE_MAIUSCOLO = 90;
static int const L = (ULTIMO_CARATTERE_MAIUSCOLO - PRIMO_CARATTERE_MAIUSCOLO) + 1;

static int const PRIMO_CARATTERE_ALFABETO = 0;
static int const ULTIMO_CARATTERE_ALFABETO = 25;

static int const MAX_BUFFER = 256;

/*
 * Prototipi
 */
void Cifra(char[], char[]);
void DeCifra(char[], char[]);

int IsCapital(char[]);
void AdattaChiave(char[], char[]);
int CalcolaAscii(int);

int main() {
    char parola[MAX_BUFFER];
    char chiave[MAX_BUFFER];

    printf("\nStringa da cifrare: ");
    // fgets al posto di scanf per evitare il troncamento delle stringhe al primo spazio
    fgets(parola, sizeof parola, stdin);
    // poiché fgets aggiunge una nuova linea, si usa strcspn per trovare il primo \n e si elimina dall'array di char
    parola[strcspn(parola, "\n")] = 0;

    printf("\nChiave di cifratura: ");
    // fgets al posto di scanf per evitare il troncamento delle stringhe al primo spazio
    fgets(chiave, sizeof chiave, stdin);
    // poiché fgets aggiunge una nuova linea, si usa strcspn per trovare il primo \n e si elimina dall'array di char
    chiave[strcspn(chiave, "\n")] = 0;

    if(!IsCapital(parola) || !IsCapital(chiave)) { // non è una stringa composta da soli caratteri maiuscoli
        printf("\nImpossibile effettuare la cifratura, le stringhe devono essere composte dai soli caratteri maiuscoli ");
    } else {

        AdattaChiave(parola, chiave);

        Cifra(parola, chiave);
        printf("stringa cifrata: %s", parola);
        DeCifra(parola, chiave);
        printf("stringa decifrata: %s", parola);

    }
    return 0;

}

/**
 * Cifra una stringa con il cifrario di Vigenére
 *
 * @param stringa_da_cifrare la stringa in chiaro da cifrare
 * @param chiave_cifratura chiave utilizzata per cifrare la stringa in input
 */
void Cifra(char stringa_da_cifrare[], char chiave_cifratura[]) {

    int len_stringa = (int) strlen(stringa_da_cifrare);
    printf("\nLunghezza stringa_da_cifrare: %d", len_stringa);

    int i = 0;
    while(i < len_stringa) {
        printf("\n\n/******************************************************\\ \n");

        printf("\nCarattere della stringa da cifrare: %d", *stringa_da_cifrare);
        printf("\nCarattere della chiave di cifratura: %d", *chiave_cifratura);

        int n = stringa_da_cifrare[i] + chiave_cifratura[i];
        printf("\nn: %d", n);

        int r = n/L;
        printf("\nr: %d", r);

        int f = CalcolaAscii(n - (L*r));
        printf("\nf: %d", f);

        stringa_da_cifrare[i] = (char) f;

        i++;
    }
    printf("\n\n/******************************************************\\ \n");

}

/**
 * Decifra una stringa con il cifrario di Vigenére
 *
 * @param stringa_da_decifrare  stringa cifrata da decifrare in chiaro
 * @param chiave_cifratura chiave utilizzata per decifrare la stringa in input
 */
void DeCifra(char stringa_da_decifrare[], char chiave_cifratura[]) {
    int len_stringa = (int) strlen(stringa_da_decifrare);
    printf("\nLunghezza stringa_da_cifrare: %d", len_stringa);

    int i = 0;
    while(i < len_stringa) {
        printf("\n\n/******************************************************\\ \n");

        printf("\nCarattere della stringa da cifrare: %d", stringa_da_decifrare[i]);
        printf("\nCarattere della chiave di cifratura: %d", chiave_cifratura[i]);

        int n = stringa_da_decifrare[i] - chiave_cifratura[i] + 26;
        printf("\nn: %d", n);

        int r = n/L;
        printf("\nr: %d", r);

        int f = CalcolaAscii(n - (L*r));
        printf("\nf: %d", f);

        stringa_da_decifrare[i] = (char) f;

        i++;
    }
    printf("\n\n/******************************************************\\ \n");
}



/************************************************************\
                     ALTRE FUNZIONI
\************************************************************/

/**
 * Controlla se una stringa è composta da soli caratteri maiuscoli sfruttando i codici ASCII
 *
 * @param stringa
 * @return -1 se la stringa non è composta da soli caratteri maiuscoli, 1 altrimenti
 */
int IsCapital(char *stringa) {
    for(int i = 0; i<strlen(stringa); i++) {
        if(stringa[i] < PRIMO_CARATTERE_MAIUSCOLO || stringa[i] > ULTIMO_CARATTERE_MAIUSCOLO) {
            return 0;
        }
    }
    return 1;
}


/**
 * In base alla lunghezza della chiave di cifratura e della stringa da cifrare,
 * uguglia la lunghezza della prima alla lunghezza della seconda:
 * - se chiave di cifratura < stringa da cifrare, copia e aggiunge a chiave di cifratura
 *      i primi (stringa da cifrare - chiave di cifratura) caratteri di chiave di cifratura
 * - se chiave di cifratura > stringa da cifrare, elimina da chiave di cifratura
 *      gli ultimi (chiave di cifratura - stringa da cifrare) caratteri
 *
 * @param parola la stringa in chiaro da cifrare
 * @param chiave_cifratura chiave utilizzata per decifrare la stringa in input
 *
 * @contributor Fabiano Golluscio
 *
 */
void AdattaChiave(char parola[], char chiave_cifratura[]) {
    // lunghezza delle stringhe
    int len_stringa = (int) strlen(parola);
    int len_chiave = (int) strlen(chiave_cifratura);

    int i=0;

    if (len_chiave < len_stringa) {
        while (len_chiave < len_stringa) {
            chiave_cifratura[len_chiave]=chiave_cifratura[i];
            len_chiave++;
            i++;
        }
    } else if(len_chiave > len_stringa) {
        while (len_chiave >= len_stringa) {
            chiave_cifratura[len_chiave]='\0';
            len_chiave--;
        }
    } else {
        // non effettua operazioni, stringa_da_cifrare e chiave_cifratura hanno lunghezza uguale
    }

}

/**
 * Calcola il codice Ascii delle lettere maiuscole a partire dal numero
 * compreso tra PRIMO_CARATTERE_ALFABETO e ULTIMO_CARATTERE_ALFABETO
 * dell'alfabeto latino
 *
 * @param a rappresenta un carattere dell'alfabeto
 */
int CalcolaAscii(int a) {
    assert(a >= PRIMO_CARATTERE_ALFABETO && a <= ULTIMO_CARATTERE_ALFABETO);

    int ascii = a + PRIMO_CARATTERE_MAIUSCOLO;
    if(ascii > ULTIMO_CARATTERE_MAIUSCOLO) {
        ascii = ascii - PRIMO_CARATTERE_MAIUSCOLO;
    }
    return ascii;
}